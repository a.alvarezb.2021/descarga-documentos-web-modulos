
import urllib.request as ur
class Robot:

    def __init__(self, url: str):
        self.url = url
        self.contenido = None #Contenido de la url inicializado a None


    def retrieve(self):
        if self.contenido is None: #Si el contenido es None, entonces se descarga la url
            print(f"Descargando url: {self.url}")
            u = ur.urlopen(self.url) #Se abre la url
            self.contenido = u.read().decode('utf-8') #Se lee el contenido y se decodifica en utf-8


    def show(self):
        self.retrieve() #Se llama a la funcion retrieve para descargar la url
        return print(self.contenido) #Se imprime el contenido

    def content(self):
        self.retrieve() #Se llama a la funcion retrieve para descargar la url
        return self.contenido #Se retorna el contenido



