#!/usr/bin/python3

import cache as c
import robot as r

if __name__ == '__main__':
    print("ROBOT CLASS")
    r_1 = r.Robot('http://gsyc.urjc.es/')
    r_2 = r.Robot('http://www.aulavirtual.urjc.es/')
    print(r_1.url)
    print(r_2.url)
    r_1.show()
    r_2.show()
    r_1.retrieve()
    r_2.retrieve()
    print("--------------------")
    print("CACHE CLASS")
    c_1 = c.Cache()
    c_2 = c.Cache()
    c_1.retrieve('http://gsyc.urjc.es/')
    c_2.retrieve('http://www.aulavirtual.urjc.es/')
    c_2.show('https://www.aulavirtual.urjc.es')
    c_1.show('http://gsyc.urjc.es/')
    c_1.show_all()
    c_2.show_all()
