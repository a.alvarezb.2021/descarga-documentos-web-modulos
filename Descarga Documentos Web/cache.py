
import robot as r
class Cache:

    def __init__(self):
        self.cache_urls = {}#Diccionario donde se almacenan las urls asociados los robots

    def retrieve(self, url):
        if url not in self.cache_urls: #Si la url no esta en el diccionario, entonces se descarga la url
            robot = r.Robot(url) #Se crea un robot con la url
            self.cache_urls[url] = robot #Se agrega la url al diccionario

    def show(self, url):
        return print(self.content(url)) #Se llama a la funcion content para imprimir el contenido de la url

    def show_all(self):
        for url in self.cache_urls: #Se recorre el diccionario
            print(url)

    def content(self, url):
        self.retrieve(url) #Se llama a la funcion retrieve para descargar la url
        return self.cache_urls[url].content() #Se retorna el contenido de la url




